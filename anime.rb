	#####################
	#      anime.rb     #
	#  17B14581 水谷 優  #
	# Copyright(C) 2017 #
	#  Encoding: UTF-8  #
	#####################

#数式処理系
def factorial(n)
	return n < 1 ? 1 : n * factorial(n-1)
end

def get_string_width(s)
	return s.each_char.map{|c| c.ascii_only? ? 1 : 2}.inject(:+)
end

def clear(only_return = false)
	if only_return == true
		return "\e[H\e[2J"
	end
	print "\e[H\e[2J"
end

def my_power(x, n)
	r = 1
	while n > 0
		if n%2>0
			r *= x
		end
		x *= x
		n /= 2
	end
	return r
end

def maclaurin_sin(x, accuracy = 20)
	r = 0
	0.upto(accuracy) do |i|
		r += (i%2>0?-1:1)*my_power(x,(2*i+1))/factorial(2*i+1)
	end
	return r
end

def maclaurin_sin_degree(x, accuracy = 20)
	return maclaurin_sin(3.14159265358979323 * ((x-180)%360+180) / 180, accuracy)
end

def move_h(x, offset = 0)
	r = ""
	if(offset >= 0)
		0.step(offset-1) do
			r += " "
		end
		return r + x
	end
	(-offset).step(x.length-1) do |i|
		r += x[i]
	end
	return r
end

def make_text_above(s, max_width, attr = "\e[32m\e[1m")	#標準: 緑色の文字、太字
	r = attr
	ot = t = ""
	w = get_string_width(s) + 2
	offset = 0
	if max_width > w
		offset = ((max_width-w)/2).floor
	end
	1.step(offset) do
		ot += " "
	end
	1.step(w) do
		t += "-"
	end
	r += ot + " " + t + "\n" + ot + "| " + s + " |\n" + ot + " " + t + "\n"
	
	return r + "\e[0m"
end

def make_text_debug(s_arr, max_width, attr = "\e[36m") #標準: アクアの文字
	r = attr
	ot = t = ""
	w = 0
	offset = 0
	s_arr.each do |s|
		wt = get_string_width(s)
		if wt > w
			w = wt
		end
	end
	if max_width > w
		offset = ((max_width-w)/2).floor
	end
	1.step(offset) do
			ot += " "
	end
	1.step(w + 2) do
		t += "-"
	end
	r += ot + t + "\n"
	s_arr.each do |s|
		r += ot + " " + s + "\n"
	end
	return r + "\e[0m"
end

#パラメータ定義
FPS = 20
TC_Wav_Speed = 13.0/21*180/FPS
TC_AA_Width = 150
TC_Wav_H_0 = 8.0/21*180/FPS
TC_Wav_H_1 = 9.0*180/FPS
TC_Wav_H_2 = 10.0*180/3/FPS
TC_Text_Vanish = 0.3*FPS	#テキスト非表示時間
#:MAXを超えると%=:MAXの処理が走る
cnt_params = [
	{:cnt => 0, :MAX => FPS * 5},	#reload
	{:cnt => 0, :MAX => FPS * 4},	#瞬き
]
#MODE: 0=>何もしない、1=>カウントアップ
trigger_params = [
	{:cnt => 0, :MODE => 1},	#表情変化
	{:cnt => 0, :MODE => 0},	#before
	{:cnt => 0, :MODE => 0},	#after
]
texts = [
	{:face=> 0, :text => "調子はどう？私は元気いっぱい！"},
	{:face=> 0, :text => "ねえねえ、遊ぼうよ"},
	{:face=> 2, :text => "えへへ ♪"},
	{:face=> 3, :text => "眠たくなってきちゃった…"},
	{:face=> 3, :text => "宿題は終わった？"},
	{:face=> 3, :text => "夜更かししちゃだめだよー？"},
	{:face=> 2, :text => "もっといろんな表情してみたいなあ"},
	{:face=> 1, :text => "人間の言葉って難しい！"},
	{:face=> 1, :text => "私の解像度が上がると 文字が小さくなっちゃうの"},
]
TC_Eye_Speed = 0.15 * FPS
TC_Eye_Phases = [
	[0, 1, 2, 1, 0],
	[],
	[],
	[1, 2, 1],
]
TC_Eye_Phase = []
text_above = "やっほ～"
text_above_space = ""
face_status = 0
previous_time = Time.now	#fps測定用
#基本表情の定義
diff_faces = [-1, 2, 4, 0]	#通常、目を閉じる、にっこり、ジト目
TC_Diff_Trans_Speed = 0.25*FPS
#表情の変化課程を定義する
#変化の順はdiff_faces[before]->diff_trans[before][after]->diff_faces[after]
diff_trans = [
	[[],     [0, 1],  [0, 3],  [], ],
	[[1, 0], [],      [1,0,3], [1],],
	[[3, 0], [3,0,1], [],      [3],],
	[[],     [1],     [3],     [], ],
]
diff_status = -1	#-1で差分なし、それ以外はdiffs[i]
pic = [
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                                                                                                  ..JUHUUZuuXUWWa..",
	"                                                                                          ...JqHHSuuuuuuuuuuuuuuuuuWh..",
	"                                                                                      ..XSuXXmkWHHHHHHHHHHHHHHmmXuuuuuX8+.  ;;;;;;;_.",
	"                                                                                    .WXQWHHyZyyyXQQQQQkyZyZyZyZXyHyzuuzuuXHJ.     ~<;<.",
	"                                                                                 `.HHWyZyZyyQkMSuuuuuzuXWHWkyZydH#XMmzuzuuuI;;;;;<- (;<",
	"                                                                              `.JHXQkHHHHHHMHHHWmmXzuuzuuzuuXHWWnHHkyMmuzuuzzzXN,<;<.",
	"                                                                            ..MyQMSuuzuuuzuzuuzuzuzuzuuzuuzzuuuXHHyUHkUNXzuz(;;;?U+;!        `",
	"                                                                           .HHqBuzuzuuzuzuuzuzuuuzuuzuzuzuuzuzuuzuWWyUHXHmuuuuun<<XHUAa++dWHt",
	"                                                                          .HdHUzuzuzuzuzuuzuzuzuzuuzuzuuzuzuuzzuuzuuWHyWHXNXzXNXuuzzuuXXQV\"     ..,",
	"                                                                          JX#uuzuuzuuzuuzuuzuuzuuzuuzuuzuuzuuuzuuzuzuuHkZHZHkXIWkzuuzuuuuuXXUUUUQY",
	"                                                                         .MSuuzuzuuzuuzuzuzuuzuzuzuzuuzuukuzzuuzuuzuzuzXNWyZWdI??mzuzzuzuQMuuuXV^",
	"                                                                        ,SuuuzuuzuzuzuuzuZO777<<<<~~~~~-((JnJ((JJwu&(JwuuHkyZdz?>zNuuzuzX5KuWS-",
	"                                                                ..dHUXuuXWHHQXzuXZ77<~~~~~~~~~((J+zwuzuuuuuuuWkuuzuuuuuzXzHkyd???jEzuuX#1jHuuzuXW9Wk=",
	"                                                             .dSuuuuuXQgHHHBY=<~~~~~~~~_((+wzuuXuuzuuzuuzzuzzuuWkuzuzzuuXHuHkd?>1duzuQ8??duuXQmQV\"`",
	"                                                           .8uuuXmY\"!.XZ=<~~~~~~~~~((uzzzzuzuzuXRuuzuuzuuQXWWWUUuHkuuzuzuuHXMW?zdHzud6???KzuzuuuXWBa,",
	"                                                         .BuXyY^    .D~~~~.~~.~(JszzzuuuuuzuuzuudRzuzuzXSuuuuuzuuzdHuuzuzuuMXMzrKHXX1?zgWSuuuXXXXQmXY\"!",
	"                                                        ,Xk\"        y~~~~.~_(zKXHuuuzuzuzuuzuuzzudmmXuuzuzuzzuuzuuuuHyuuzuzuNHkMMN@zQdNXVVVVVfkY^",
	"                                                       ,Y!         .>~~._(zuzXHHUMkzuuzuzuzuzuuzuuds~~?7\"\"\"TTUXuXXXXXdkuuzuudKyyZyWMfVffffWWHh.._",
	"                                                      .^           y~~(Juuzuud~~~~_Wmyuuuzuuzuzuzuzde   ?  .+MMMMMNg,.?kuuzzuHXQkZy#HB90O???jJ\"",
	"                                                                  .F(wuuuzuzud~-(^ `.TkzuuzuuzuuzuuzXp  .JM5!.d9XXVhJMe?kuuzudHbMyyNWkmggggdh.",
	"                                                                  .Ruzuuzuzuzd:_       7kuzuzuuzuzuuuXL 'M% .Srvvrvrd2Tbjzuuzu#WMydHffVVffffffWN,.",
	"                                                                  -XuuzuXzuuud__     ?7^`?XXuzuuukzuzuXh J{`KrvrvZ7777,M,Kzuuz#HkdNfffVVWWkWYY\"\"\"T=",
	"                                                                  -uzuuXSuzuzdl_    .gMBWMMMHkuzuXNWmXuuN.  jvrvrvw&..[? #uzuu].(HfVffWN,",
	"                                                   .@(.           ,kuzuKuzuuzXF_   J@~(SvvXS#)?4muuWJ__7\"T.  Wwvvrvvrd!  WzuzXmNfffffffffWNJ.",
	"                                                   -:` 74(,.   .774KuuXUuuzuuuMx  .M!.SvrrrvZt   (TAyH,       (TXwwY\"    XuzudWfffffffffffffWN,",
	"                                                   ?J,`````.?4$    NzX#uzuzuzudJn  N-.KvvO>...l      _\"5-       .._~~~~_`XzuuNffffpWkVY\"\"\"\"\"\"YWb.",
	"                                                      ?6, ``.%    .MKduuzuuzuzXb:4 ?N-?yvrvvvvP                ___      .#uzdNfffffN,",
	"                            .....(<<<-                .J^```v     ,:N#uzuzuuyuud/<Jp.\"-?WyvwQf                        .dHSuX#Nde?\"\"UN.",
	"                            /(....../1T.            .\" ````.\    .F~dUuuzuzuVkzuMWJ,b                               .JNpHzudHfHmU,",
	"                          .%,Z6Cv17I_i((h,        ,=```````.~    .>:dzuuuzuzuWNXXNffT:_~~~~_               ,      .J=~~(SXf` ?\"T7sU,",
	"                          ,/6!`.`(iG+~..jZ\     .3``````` .(_    .:(#uzzuuzuX#fWHXN&(,_                 .y7    ..Y~~71JBQYjn      ?AU-",
	"                           .i.7WWVAJ4n  -d,   .Y`````` ._::?|    .-(SuuXSuuzddWVVWNNkN,           `-~~``     .J=(4J?7(HV!  ~?&.     ?Wdn.",
	"                             .i ?1/63(I,?;j   .\```..JJZ\"\"7!G    .L(uzuXkzuX@+NfVVVWWHHMmJ..            ...T=:(/=  .J\"  .?4s<~4       .Ty9.",
	"                               7Alllzuz7o;j  .t .(Z\"!        4-~~-J4uzuXRuzd!  4WVV#TSkfWr:(?TTTTT7T#f<~(>_(J^      ..d```` 4,(`         ?Oz6..",
	"                                 .4szzzo+XC) .mJ=             .7\"' ,uuuuKuuX    ?kVH  .4WF\"TaJ-:::(k??I .e\" ...?7TT1??b.`````,L             ?4&T&.",
	"                                     ?4yIz<4.                      ,zzuzHzzP      7W[   ?].(k\" ?=uJ1?! uQ?=     ??????N~ ````` 4.              .71xTi.",
	"                                         ?4&6.                     .Kuuzdku]         ! ..Wv^     .6?>          (???=??dp~```````?,                 .7uxTi.",
	"                                            7&?i,                   WuuuuHu]        .-Xv^        f?=`         ,=??=?=?zde~.``````(,                    .\"uxYi.",
	"                                              .7+zi,                ,kzuzdkb    ..Ox7!           b?<         .??=????1Xvdh:.``````,kzzzwwuZX1.             .\"uzT(.",
	"                                                  71JC(..            ?kuuzWd.+Xx=!              .$?:         +???=?==ZvvP TJ_`````.' ?:~-../?,i                _7uv6.",
	"                                                     .77++ZC11+......-dmuuuW@                  ..1?`        .??=??=?dwvd!  ,n<.``.r,111jJ,d-(i1?Z(.                ?1J6,",
	"                                                              ~?!!?!`  ,WXuzXn.              .K=?<!         (??=???j6vwF     ?x-`.4YY\"\"\"Tfddn,?.GF7                   7xT,",
	"                                                                         .4QXuVh.        ..J9C???>         ?wmO?=??SvvX        4-.`.4,?4x4xZn=.jCJ+                     4Jl",
	"                                                                             ?\"\"\"T\"~...T6?????=?<`        .??=v3?=1dvd!         /e-```?u.JwGG.A$>1+n                     1J.",
	"                                                                               ..JT6?????<<??=<!          (????=??=?zvW,          4+.```(7WwU0VTG+jY;                    Jj`",
	"                                                                         ...ZT1???????=?.                 (=?=??=????wvd           (e_ ``` Tf\"4pXXIvj+..               .Zu\"",
	"                                                                  ...Jv61??????=??=?=??=:                  <??=??=?=?wvd,            4+.````(h.  ??\"?\"4&zTC1(.....-zOTux\"",
	"                                                          ....zv61==??????=??=??=??=??=<`                   -??=?????vkzW,            ,n_.````7,           ?777?7??!`",
	"                                                  ..(?77???????77777\"\"71zu&&&z??=???<?!                      .+?=?=?=?dyzH.             4+-````.4,",
	"                                                ,^                              ??77<....                .(=?=???=?????dyzN.             ,n<.````.S.(\"^`  ?4,",
	"                                              .^         ........                         _77<...        (=??=?=??=?=??=dwvh               TJ<. .?'         ,[",
	"                                             .%   ..JXSvrvr~~~~~~~~~~~<4U9UXG+.....              .7^(..  .=???=??=??=?=??4wzh               ,hJ=         ...:r",
	"                                             ,  .<Xrrrrrrrd/~~~~~~~~~_(JJ\"=!?TkyZyyyWHWA(..            _7<jx?=??=??=?????=Wwzh              .=       ..7ThJ:J'",
	"                                             (   ~JyrrvrvvvZo~~(JJdWBJ<.`````` 4ZZZZyZZXdBvvX90+..          _7Ox??=??=?=??=Wwum..          .'     .,3<~``` 7G.",
	"                                             .[  ~~?yrvvwQgXH9UuuuuuuuXl?.`````.HyyZXkBXvvvvrvvvvvZU0J.         .7Ox??=????zWzuNtZS(.      .,   .de;~```````` TG,.",
	"                                              .n .(JJMHUuuuuzuzzuzzuzuuuL(`````.HXkBXvvvvvvvvvvvvvvvdWyyHk-.        (=&z=??=zWzzNAyttZS..   ?,..=:?W+-```````````(\"\"Su......",
	"                                          ...JVHWuuuuuzuzuzuuzuuzuuzuzuzX._```.J5~(TwwvvvvvvvvvvvvzV\"<??TXyyWH&.        7wz??zWzzh ?TAOtOU+.  ?7\"7` (n< `````````````````.V`",
	"                                     .dHWUuuuuzuuuuzuzuzuuzuuzuuzuuzuuzuZ. .J=~~~~~~~~?TwwrvvvvvvQY```````(WZyHzXS(.       7GzOUzzN.   7XytrU-.       7J-``..J++zz1z<17C7=",
	"                                    dSuuzuzzuzuuuzzuzuuzuuzuzuzuuzzuzuuQaHhJ~~~~~:~~~~~~~~?74wwvd=`````````.HdSzzzzzUn,       7XwzzW,     7AO>?G.       4+.(~",
	"                                    #uuzuuzuuzuuzuuuzuuzuuzuuzuzuuuXgH8vvvvvZU&.~~~:~~:~~~~~~~(J<<??!. `````-BzzzzzzzzzZS..     .7mzd,      ,4+;<6.      ,\"\"",
	"                                    vkuuzuuzuuzuuzuzuzuuzuuuzuuXQWMXvvvvrvvvvvvzZUuJ_~~:~:~:~(BUuuzuUa,i ``.#zzzzzzzzzzzzzZS.      (4dn       .4<;?n",
	"                                     4kzuzuzuzuuuzuuzuzuzuzXQK5:~~~(TXwvvrvrvvvvvvvvvUX+J_~(dSuuuzuzzuX,?. y7TTwwwwwwwwwwZY=:?3,      7h.       (x:;v,",
	"                                      /muuzuuzuuzuzuuzuXQV\"!  7&. _~~~~?4wvvvvvvvvvvzzzvzzWBuuzuzuuuzuzX[1J<~:~::~:~::~::~::~:~:?i.     7,       .n:;?,",
	"                                        7HyuzuzuuzuQmY\"!         7+, `_~~~(7UyvvvvzzzvvzQBuuzuuzuzuzuuuuX,>~:~:~::~:~:~:~:~::~:::(dS,    .o       .y;:?,",
	"                                           7YWkWT\"=                 ?=.  _~~~~(74wwvzzQHXuzuuzuuzuzuzuzudhJ--:~~~:~::~:~:~::~(JJUzzuuh,    I       ,<;:u",
	"                                                                        7i. `_~~~~~?XHUuuzuzuzuzuuuzuzuQEzzzzzXUUU0zzzzzuXUUXzuzzuzzzuU,  ..[       P:;?|",
	"                                                                           .7(. `_(WUuuzuuzuzuuzuzuuzuX#zzzzzzzuzzzzzuzuzuzuzuzuuzuuuuXb .: r       P;:;]",
	"                                                                               (QHuzuzuzuzuuzuzuzuzuuXHzzzzzzzzzzzuzuzuzuzuzuzuzuuzuXVCP::~.%      .$:;:]",
	"                                                                             .JBuzzuzuzuuzuzuuuzuuzuud?7TTXwmwuuzuzuzuzuzuzuzuXXXY=<::?::~.^       ,;:;+%",
	"                                                                          ..MSuzuuuuuzuuzuuzuzuuzuuzd>~::~::~::~<?7777777777<::::::::::~.7        .5;::J",
	"                                                                         (Buuzuuzzuzuuzuuzuuzuzuzuzd3~:~:~::~::::::::::::::::::~~~~~(.?'         ,7?71+]",
	"                                                                        -Szuzuzuuzuzuzuzuzuzuuzuuzdt  ??77<(....    ```     ....?7!           ,SOOttU&,.",
	"                                                                        duuzuuzuzuuuzuuzuuzuzuuzuqF                  `                        K'   .7ydL",
	"                                                                        (KuzuzuuzuzuuzuuzuuzuzuzQF                                          .7???-.  (HW",
	"                                                                         WXuuzuzuzuzuzuzuzuuzuuQF                                          ,`       1+f`",
	"                                                                          4yuuzuuuzuuuzuuzuzuuXP                                         .=         ;J:",
	"                                                                           ?HXuzuzuzuzuuzuzuzQP                                         .Jxz+...   (:J",
	"                                                                             ?NmzuuzuuzuuzuuqF                                        ,6ttttttttZn,::v",
	"                                                                                \"WmXuzuzuuXd^                                       iSwUWfWUUSAyttdHJ]",
	"                                                                                    `\"\"\"\"\"'                                         dd9SuuuuXWXsVAXyW%",
	"                                                                                                                                    Wb:?WukkXVkC7HHky]",
	"                                                                                                                                    :4m::zD?>d6??jNWW\"",
	"                                                                                                                                      ?Waf??jY77>:jWF",
	"                                                                                                                                        J??1mJ++&WY'",
	"                                                                                                                                       J???v",
	"                                                                                                                                      ,1??d!",
	"                                                                                                                                     .1?>j%",
	"                                                                                                                                    uC??1r",
	"                                                                                                                                    ?x??d",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
	"                    ",
]
#差分
#top: 上端の行数, diff: 差分テキスト
diffs = [
	{:top => 30, :diff => [
		"                                                       ,Y!         .>~~._(zuzXHHUMkzuuzuzuzuzuuzuuds~~?7\"\"\"TTUXuXXXXXdkuuzuudKyyZyWMfVffffWWHh.._",
		"                                                      .^           y~~(Juuzuud~~~~_Wmyuuuzuuzuzuzuzde   ?   +         ?kuuzzuHXQkZy#HB90O???jJ\"",
		"                                                                  .F(wuuuzuzud~-(^   TkzuuzuuzuuzuuzXp                 ?kuuzudHbMyyNWkmggggdh.",
		"                                                                  .Ruzuuzuzuzd:_       7kuzuzuuzuzuuuXL      .+MMMMMNgz jzuuzu#WMydHffVVffffffWN,.",
		"                                                                  -XuuzuXzuuud__         ?XXuzuuukzuzuXh    ,JvrvZ7777,M,Kzuuz#HkdNfffVVWWkWYY\"\"\"T=",
		"                                                                  -uzuuXSuzuzdl_            HkuzuXNWmXuuN.  KJrvrvw&..[? #uzuu].(HfVffWN,",
		"                                                   .@(.           ,kuzuKuzuuzXF_         __,   4muuWJ__7\"T.  Wwvvrvvrd!  WzuzXmNfffffffffWNJ.",
		"                                                   -:` 74(,.   .774KuuXUuuzuuuMx     .gMBWMMMt   (TAyH,       (TXwwY\"    XuzudWfffffffffffffWN,",
		"                                                   ?J,`````.?4$    NzX#uzuzuzudJn   +.KvvO>...l      _\"5-       .._~~~~_`XzuuNffffpWkVY\"\"\"\"\"\"YWb.",
		"                                                      ?6, ``.%    .MKduuzuuzuzXb:4  N-?yvrvvvvP                ___      .#uzdNfffffN,",
		"                            .....(<<<-                .J^```v     ,:N#uzuzuuyuud/<Jp.\"-?WyvwQf                        .dHSuX#Nde?\"\"UN.",
	]},
	{:top => 30, :diff => [
		"                                                       ,Y!         .>~~._(zuzXHHUMkzuuzuzuzuzuuzuuds~~?7\"\"\"TTUXuXXXXXdkuuzuudKyyZyWMfVffffWWHh.._",
		"                                                      .^           y~~(Juuzuud~~~~_Wmyuuuzuuzuzuzuzde   ?   +         ?kuuzzuHXQkZy#HB90O???jJ\"",
		"                                                                  .F(wuuuzuzud~-(^   TkzuuzuuzuuzuuzXp                 ?kuuzudHbMyyNWkmggggdh.",
		"                                                                  .Ruzuuzuzuzd:_       7kuzuzuuzuzuuuXL                 jzuuzu#WMydHffVVffffffWN,.",
		"                                                                  -XuuzuXzuuud__         ?XXuzuuukzuzuXh    ,Jyva, ..,/M,Kzuuz#HkdNfffVVWWkWYY\"\"\"T=",
		"                                                                  -uzuuXSuzuzdl_            HkuzuXNWmXuuN.  KJrvrvw&..[? #uzuu].(HfVffWN,",
		"                                                   .@(.           ,kuzuKuzuuzXF_               4muuWJ__7\"T.  (TXwwY\"    WzuzXmNfffffffffWNJ.",
		"                                                   -:` 74(,.   .774KuuXUuuzuuuMx                 (TAyH,                   XuzudWfffffffffffffWN,",
		"                                                   ?J,`````.?4$    NzX#uzuzuzudJn   +.    _ .,l      _\"5-       .._~~~~_`XzuuNffffpWkVY\"\"\"\"\"\"YWb.",
		"                                                      ?6, ``.%    .MKduuzuuzuzXb:4  N-?yvrvvvvP                ___      .#uzdNfffffN,",
		"                            .....(<<<-                .J^```v     ,:N#uzuzuuyuud/<Jp.\"-?Wyvw`                         .dHSuX#Nde?\"\"UN.",
		"                            /(....../1T.            .\" ````.\    .F~dUuuzuzuVkzuMWJ,b                               .JNpHzudHfHmU,",
		"                          .%,Z6Cv17I_i((h,        ,=```````.~    .>:dzuuuzuzuWNXXNffT:_~~~~_               ,      .J=~~(SXf` ?\"T7sU,",
		"                          ,/6!`.`(iG+~..jZ\     .3``````` .(_    .:(#uzzuuzuX#fWHXN&(,_                 .y7    ..Y~~71JBQYjn      ?AU-",
	]},
	{:top => 30, :diff => [
		"                                                       ,Y!         .>~~._(zuzXHHUMkzuuzuzuzuzuuzuuds~~?7\"\"\"TTUXuXXXXXdkuuzuudKyyZyWMfVffffWWHh.._",
		"                                                      .^           y~~(Juuzuud~~~~_Wmyuuuzuuzuzuzuzde   ?   +         ?kuuzzuHXQkZy#HB90O???jJ\"",
		"                                                                  .F(wuuuzuzud~-(^   TkzuuzuuzuuzuuzXp                 ?kuuzudHbMyyNWkmggggdh.",
		"                                                                  .Ruzuuzuzuzd:_       7kuzuzuuzuzuuuXL                 jzuuzu#WMydHffVVffffffWN,.",
		"                                                                  -XuuzuXzuuud__         ?XXuzuuukzuzuXh              ,M,Kzuuz#HkdNfffVVWWkWYY\"\"\"T=",
		"                                                                  -uzuuXSuzuzdl_            HkuzuXNWmXuuN.  KJrAv, jK7[? #uzuu].(HfVffWN,",
		"                                                   .@(.           ,kuzuKuzuuzXF_               4muuWJ__7\"T.  (TXwwY\"`   WzuzXmNfffffffffWNJ.",
		"                                                   -:` 74(,.   .774KuuXUuuzuuuMx                 (TAyH,                   XuzudWfffffffffffffWN,",
		"                                                   ?J,`````.?4$    NzX#uzuzuzudJn             ,      _\"5-       .._~~~~_`XzuuNffffpWkVY\"\"\"\"\"\"YWb.",
		"                                                      ?6, ``.%    .MKduuzuuzuzXb:4  N-v,  _anvP                ___      .#uzdNfffffN,",
		"                            .....(<<<-                .J^```v     ,:N#uzuzuuyuud/<Jp.\"-?Wyvw`                         .dHSuX#Nde?\"\"UN.",
		"                            /(....../1T.            .\" ````.\    .F~dUuuzuzuVkzuMWJ,b                               .JNpHzudHfHmU,",
		"                          .%,Z6Cv17I_i((h,        ,=```````.~    .>:dzuuuzuzuWNXXNffT:_~~~~_               ,      .J=~~(SXf` ?\"T7sU,",
		"                          ,/6!`.`(iG+~..jZ\     .3``````` .(_    .:(#uzzuuzuX#fWHXN&(,_                 .y7    ..Y~~71JBQYjn      ?AU-",
	]},
	{:top => 30, :diff => [
		"                                                       ,Y!         .>~~._(zuzXHHUMkzuuzuzuzuzuuzuuds~~?7\"\"\"TTUXuXXXXXdkuuzuudKyyZyWMfVffffWWHh.._",
		"                                                      .^           y~~(Juuzuud~~~~_Wmyuuuzuuzuzuzuzde   ?   +         ?kuuzzuHXQkZy#HB90O???jJ\"",
		"                                                                  .F(wuuuzuzud~-(^   TkzuuzuuzuuzuuzXp                 ?kuuzudHbMyyNWkmggggdh.",
		"                                                                  .Ruzuuzuzuzd:_       7kuzuzuuzuzuuuXL      .+MMMMMNg  jzuuzu#WMydHffVVffffffWN,.",
		"                                                                  -XuuzuXzuuud__         ?XXuzuuukzuzuXh    ,JvrvZwwYx   Kzuuz#HkdNfffVVWWkWYY\"\"\"T=",
		"                                                                  -uzuuXSuzuzdl_            HkuzuXNWmXuuN.  KJTW`        #uzuu].(HfVffWN,",
		"                                                   .@(.           ,kuzuKuzuuzXF_         __,   4muuWJ__7\"T. `            WzuzXmNfffffffffWNJ.",
		"                                                   -:` 74(,.   .774KuuXUuuzuuuMx     .gMBWMMMt   (TAyH,                   XuzudWfffffffffffffWN,",
		"                                                   ?J,`````.?4$    NzX#uzuzuzudJn   +.KvvO>wgp       _\"5-       .._~~~~_`XzuuNffffpWkVY\"\"\"\"\"\"YWb.",
		"                                                      ?6, ``.%    .MKduuzuuzuzXb:4   -?f`                      ___      .#uzdNfffffN,",
		"                            .....(<<<-                .J^```v     ,:N#uzuzuuyuud/<Jp.                                  .dHSuX#Nde?\"\"UN.",
		"                            /(....../1T.            .\" ````.\    .F~dUuuzuzuVkzuMWJ,b                               .JNpHzudHfHmU,",
		"                          .%,Z6Cv17I_i((h,        ,=```````.~    .>:dzuuuzuzuWNXXNffT:_~~~~_               ,      .J=~~(SXf` ?\"T7sU,",
		"                          ,/6!`.`(iG+~..jZ\     .3``````` .(_    .:(#uzzuuzuX#fWHXN&(,_                 .y7    ..Y~~71JBQYjn      ?AU-",
	]},
	{:top => 30, :diff => [
		"                                                       ,Y!         .>~~._(zuzXHHUMkzuuzuzuzuzuuzuuds~~?7\"\"\"TTUXuXXXXXdkuuzuudKyyZyWMfVffffWWHh.._",
		"                                                      .^           y~~(Juuzuud~~~~_Wmyuuuzuuzuzuzuzde   ?   +         ?kuuzzuHXQkZy#HB90O???jJ\"",
		"                                                                  .F(wuuuzuzud~-(^   TkzuuzuuzuuzuuzXp                 ?kuuzudHbMyyNWkmggggdh.",
		"                                                                  .Ruzuuzuzuzd:_       7kuzuzuuzuzuuuXL      .+MMMMMNg  jzuuzu#WMydHffVVffffffWN,.",
		"                                                                  -XuuzuXzuuud__         ?XXuzuuukzuzuXh    ,J{`    `va  Kzuuz#HkdNfffVVWWkWYY\"\"\"T=",
		"                                                                  -uzuuXSuzuzdl_            HkuzuXNWmXuuN.  K/           #uzuu].(HfVffWN,",
		"                                                   .@(.           ,kuzuKuzuuzXF_         __,   4muuWJ__7\"T.              WzuzXmNfffffffffWNJ.",
		"                                                   -:` 74(,.   .774KuuXUuuzuuuMx     .gMBWMMMt_  (TAyH,                   XuzudWfffffffffffffWN,",
		"                                                   ?J,`````.?4$    NzX#uzuzuzudJn   +.K      ra      _\"5-       .._~~~~_`XzuuNffffpWkVY\"\"\"\"\"\"YWb.",
		"                                                      ?6, ``.%    .MKduuzuuzuzXb:4  K}                         ___      .#uzdNfffffN,",
		"                            .....(<<<-                .J^```v     ,:N#uzuzuuyuud/<J                                    .dHSuX#Nde?\"\"UN.",
		"                            /(....../1T.            .\" ````.\    .F~dUuuzuzuVkzuMWJ,b                               .JNpHzudHfHmU,",
		"                          .%,Z6Cv17I_i((h,        ,=```````.~    .>:dzuuuzuzuWNXXNffT:_~~~~_               ,      .J=~~(SXf` ?\"T7sU,",
		"                          ,/6!`.`(iG+~..jZ\     .3``````` .(_    .:(#uzzuuzuX#fWHXN&(,_                 .y7    ..Y~~71JBQYjn      ?AU-",
	]},
]

#メインループ
0.step do |i|
	#テックちゃん処理系
	p = []
	##揺れ処理
	offset_v = (maclaurin_sin_degree(TC_Wav_Speed*i)*9).round
	offset_h = 0
	##テキスト処理
	if cnt_params[0][:cnt] < FPS * 0.3 && i != cnt_params[0][:cnt]
		if cnt_params[0][:cnt] == 0
			next_text = texts.reject{|t| t[:text] == text_above}.sample
			text_above = next_text[:text]
			trigger_params[0][:cnt] = 0
			trigger_params[2][:cnt] = next_text[:face]
		end
		if [1,0,0,0,0].sample > 0 && [0,2].include?(trigger_params[2][:cnt])
			offset_h = maclaurin_sin_degree(TC_Wav_H_1*cnt_params[0][:cnt])\
			* maclaurin_sin_degree(TC_Wav_H_1*cnt_params[0][:cnt])\
			* 6
		end
	end
	offset_h += (maclaurin_sin_degree(TC_Wav_H_0*i)*6).round
	#画面処理系
	##表情処理(遷移)
	trans_t = diff_trans[trigger_params[1][:cnt]][trigger_params[2][:cnt]]
	if trigger_params[0][:cnt] < (TC_Diff_Trans_Speed * trans_t.length).floor
		diff_status = trans_t[trigger_params[0][:cnt] / TC_Diff_Trans_Speed]
	elsif trigger_params[0][:cnt] == (TC_Diff_Trans_Speed * trans_t.length).floor + 1
		trigger_params[1][:cnt] = trigger_params[2][:cnt]
		face_status = trigger_params[2][:cnt]
	else
		diff_status = diff_faces[face_status]
	end
	##表情処理(瞬き)
	if cnt_params[1][:cnt] == 0
		TC_Eye_Phase = TC_Eye_Phases[face_status]
	end
	if cnt_params[1][:cnt] < TC_Eye_Speed * TC_Eye_Phase.length && i != cnt_params[1][:cnt]
		diff_status = TC_Eye_Phase[cnt_params[1][:cnt] / TC_Eye_Speed]
	end
	0.step(pic.length-1) do |i|
		if i < offset_v
			p << ""
		elsif pic.length <= i-offset_v
			p << ""
		else
			##表情処理(描画)
			if diff_status != -1 && diffs.length > diff_status
				top = diffs[diff_status][:top]
				p << move_h((top-1<=i-offset_v&&top+diffs[diff_status][:diff].length-1>i-offset_v \
				? diffs[diff_status][:diff][i-offset_v-top+1]:pic[i-offset_v])\
				, offset_h)
			else
				p << move_h(pic[i-offset_v],offset_h)
			end
		end
	end
	#描画処理系
	pr_t = ""
	pr_t += clear(true)+"\n\n"
	##テキスト処理
	if cnt_params[0][:cnt] < cnt_params[0][:MAX]-TC_Text_Vanish
		pr_t += make_text_above(text_above, TC_AA_Width)
	else
		pr_t += "\n\n\n"
	end
	##描画処理
	0.step(p.length-1) do |i|
		pr_t += p[i] + "\n"
	end
	tmp_tn = Time.now
	time_elapsed = tmp_tn - previous_time
	previous_time = tmp_tn
	pr_t += "\n" + make_text_debug([
		"fps: " + sprintf("%.2f", 1.0/time_elapsed),
	], TC_AA_Width)
	$> << pr_t
	#周期パラメータ処理
	0.step(cnt_params.length-1) do |i|
		cnt_params[i][:cnt] = (cnt_params[i][:cnt]+1) % cnt_params[i][:MAX]
	end
	0.step(trigger_params.length-1) do |i|
		if trigger_params[i][:MODE] == 1
			trigger_params[i][:cnt] += 1
		end
	end
	sleep(1.0/FPS)
end
